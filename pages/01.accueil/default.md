---
hide_page_title: false
show_sidebar: true
hide_git_sync_repo_link: false
---

Bonjour

Ce blog est le recueil d'informations au sujet de mes activités associatives, professionnelle, personnelles

Le code source est disponible et modifiable sur [FRAMAGIT](https://framagit.org/galileo/blog)

Dernières bidouilles :

[détecteur de Co2](http://nousaerons.fr/makersco2/) : pour l'école de mon village

[alcasar](https://alcasar.net/) installation d'un logiciel de contrôle d'accès pour le partage d'une connexion fibre par plusieurs associations

[tile-cache](http://tilecache.org/docs/README.html) pour accélérer l'affichage de tuiles cartographiques 

[wsusoffline](https://download.wsusoffline.net/) : pour accélérer le déploiement de postes window$
