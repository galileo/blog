---
title: Accueil
content:
    items: '@self.children'
    limit: '5'
    order:
        by: date
        dir: desc
    pagination: '1'
---

Bonjour

Ce blog est le recueil d'informations au sujet de mes activités de conseiller municipal.

Vous y trouverez aussi des informations récoltées au sujet des logiciels libre et de l'énergie.

Le code source est disponible et modifiable sur [FRAMAGIT](https://framagit.org/galileo/blog)

[wallabag](https://valog.fr/wallabag/)

