---
title: FAI
hide_page_title: false
show_sidebar: true
hide_git_sync_repo_link: false
content:
    items: '@self.children'
    limit: 5
    order:
        by: date
        dir: desc
    pagination: '1'
    url_taxonomy_filters: true
hero_scroll: false
hide_post_summary: false
hide_post_date: false
hide_post_taxonomy: false
continue_link_as_button: false
---

Je participie craéation du Fournisseur d'Accès Associatif (FAI) [TILiMa](https://tilima.fr) 
Vous trouverez ici quelques outils et articles descriptifs.