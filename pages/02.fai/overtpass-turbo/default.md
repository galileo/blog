---
title: overtpass-turbo
date: '15-06-2017 00:00'
hide_page_title: false
show_sidebar: true
hide_git_sync_repo_link: false
content:
    items: '@self.children'
    limit: '5'
    order:
        by: date
        dir: desc
    pagination: '1'
    url_taxonomy_filters: true
---

overpass-turbo est un outil qui permet d'extraire des donnée d'une carte openstreetmap en utilisant une requête.

Cet article présente la méthode de localisation des points haut qui peuvent permettre d'installer des antennes WIFI pour la couverture des zones Blanches et Grises.

<a href="http://overpass-turbo.eu/" title="http://overpass-turbo.eu/">http://overpass-turbo.eu/</a> 


```

/*
This query looks for nodes, ways and relations 
with the given key/value combination.
Choose your region and hit the Run button above!
*/
[out:json][timeout:25];
// gather results
(
  	// query part for: “man_made=water_tower”
  	node["man_made"="water_tower"]({{bbox}});
	way["man_made"="water_tower"]({{bbox}});
	relation["man_made"="water_tower"]({{bbox}});

	node["amenity"="place_of_worship"]({{bbox}});
	way["amenity"="place_of_worship"]({{bbox}});
    relation["amenity"="place_of_worship"]({{bbox}});

	node["amenity"="townhall"]({{bbox}});
    way["amenity"="townhall"]({{bbox}});
    relation["amenity"="townhall"]({{bbox}});
);

// print results
out body;
>;;
out skel qt;
```
