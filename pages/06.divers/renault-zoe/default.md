---
title: 'Renault ZOE'
hide_page_title: false
show_sidebar: true
hide_git_sync_repo_link: false
content:
    items: '@self.children'
    limit: '5'
    order:
        by: date
        dir: desc
    pagination: '1'
    url_taxonomy_filters: true
---

Charge en fonction de l'intensité
http://renault-zoe.forumpro.fr/t1653-voici-comment-charge-la-zoe-en-fonction-de-lintensite

| Ampères | temps | rendement | informations |
| ----------- | ----------- | ----------- | ----------- |
|6A           |               |               | Charge quasi impossible|
|8A           |   30h      | 50% | 22kWh -> 44kWh soit le plein à 44x0,12 = 5,28€ |
|10A         | 18h        | 70% | 22kWh -> 31,5kWh soit le plein à 31,5x0,12 = 3,78€ |
|13A         | 11h        | 80% ||
|30A         | 4.5h       | 80% ||

Temps de charge en 10A
http://renault-zoe.forumpro.fr/t1446-temps-de-recharge-effective-a-10a-teste-avec-le-cro-de-davidr#21452


|Temps de charge| % de charge | Km possible |
| --------------------- | ----------------- | ---------------- |
| 1             | 5,8         |   6,8          |
| 2             | 11,5        |  13,5       |
| 3             | 17,3        |  20,3       |
| 4             | 23,0        |  27,0       |
| 5             | 28,8        |  33,8       |
| 6             | 34,5        |  40,5       |
| 7             | 40,3        |  47,3       |
| 8             | 46,0        |  54,0       |
| 9             | 51,8        |  60,8       |
| 10            | 57,5        |  67,5       |
| 11            | 63,3        |  74,3       |
| 12            | 69,0        |  81,0       |
| 13            | 74,8        |  87,8       |
| 14            | 80,5        |  94,5       |
| 15            | 86,3        |  101,3      |
| 16            | 92,0        |  108,0      |
| 17            | 97,8        |  114,8      |
| 18            | 100,0       |  120,0      |


Brochure ZOE

https://www.cdn.renault.com/content/dam/renault/fr/brochures/X101VEVPZOE_STD/fr/fr_FR_X101VEVPZOE_STD.pdf
