---
title: 'System Rescue CD sur une clé USB'
hide_page_title: false
show_sidebar: true
hide_git_sync_repo_link: false
---

Le but de ce tutoriel est d'expliquer comment installer Systm Recue CD sur une clÃ© USB

<span style="color:red">ATTENTION : Une mauvaise manipulation peut entraîner une perte de donnée sur les périphérique de votre ordinateur, y compris votre disque dur. Soyez donc très prudent !</span>

Les commandes à saisir sont précédées d'un : $


# 1 GPARTED

en ligne de commande

```
$ sudo gparted
[sudo] Mot de passe de utilisateur :
```

ou par le menu de votre bureau

<span style="color:red">Ici est le point le plus important! Veillez à sélectionner le périphérique de votre clé USB  sous peine de perdre toutes les données de votre disque DUR!</span>

Sélectionnez le périphérique correspondant à votre clé USB. Dans mon cas il s'agit de /dev/sdb.

![](01.png)


Supprimez la partition existante

![](02.png)


Créez en une nouvelle

![](03.png)


Sélectionnez le système de fichier FAT32 et éventuellement une étiquette.

![](04.png)


Ne modifiez pas les autres paramètres si vous ne savez pas ce que vous faites.
Puis cliquez sur Ajouter.

![](05.png)


Appliquez les changements

![](06.png)


GPARTED demande de confirmer votre action car vous ne pourrais pas revenir en arrière

![](07.png)


Rendre la clé bootable

![](08.png)

![](09.png)

Après validation vous pouvez quiter GPARTED

# 2 - Utilitaire de création

Ouvrez un terminal
Créez un point de montage

```sh
$ sudo mkdir /mnt/iso
[sudo] Mot de passe de utilisateur :
```

Téléchargez systemRescueCD sur http://www.system-rescue-cd.org/Download/

Montez l'image iso de SYSRCD dans votre point de montage

```sh
$ sudo mount systemrescuecd-x86-5.0.0 /mnt/iso
mount: /dev/loop0 is write-protected, mounting read-only
```

si tout a bien fonctionné vous pouvez lister le contenu du fichier ISO

```sh
$ ls /mnt/iso
boot  bootdisk  efi  isolinux  ntpasswd  readme.txt  sysrcd.dat  sysrcd.md5  usb_inst  usb_inst.sh  usbstick.htm  version
```

si le fichier "usb_inst.sh" apparaît, vous pouvez lancer le processus d'installation

```sh
$ sudo /mnt/iso/usb_inst.sh
[sudo] Mot de passe de utilisateur :
```

Si vous obtenez un message d'erreur


```sh
Device [/dev/sdb] detected as [Kingston DataTraveler G3 ] is removable and size=3818MB
* Device [/dev/sdb] is mounted: cannot use it
All valid USB/Removable devices are currently mounted, unmount these devices first
```

Vous devez démonter la clé USB


```sh
$ sudo umount /dev/sdb1
```

Puis relancer l'utilitaire

```sh
$ sudo /mnt/iso/usb_inst.sh
[sudo] Mot de passe de utilisateur :
```

une boîte de dialogue devrait apparaître :
![](10.png)


Sélectionnez le périphérique correspondant à votre clé USB.
La souris n'étant pas utilisable, vous devez utiliser la barre d'espace du clavier pour sélectionner la clé USB et la touche "entrée" pour valider votre choix.

Le processus d'installation durera 1 à 2 minutes suivant les performance de votre machine et la bande passante de votre prise USB.

![](11.png)

A la fin de l'opération, un message vous confirme que tout s'est bien passé.

![](12.png)

vous pouvez retirer la clé USB

Utilisation
Insérez la clé USB dans la prise de l'ordinateur vous voulez utiliser SYSRCD
Utilisez la touche de fonction qui permet de sélectionner votre périphérique de démarrage.
Cette touche diffère suivant les constructeurs (ESC, F2, F8, F9, F10, F11, F12)
Reportez vous à la documentation de votre machine ou essayez les touches les unes après les autres.

Sélectionnez le périphérique qui correspond à votre clé USB puis validez

![](14.jpg)

Le menu de démarrage de SYSRCD devrait apparaître.
sans action pendant plus de 90 secondes, le SYSRCD démarrera avec l'option par défaut.

![](13.png)

4 FIN
Félicitation, vous avez en main une clé LiveUSB  vous permettant de nombreuses manipulation sur votre machine.