---
title: 'Raspberry Pi Minecraft'
hide_page_title: false
show_sidebar: true
hide_git_sync_repo_link: false
content:
    items: '@self.children'
    limit: '5'
    order:
        by: date
        dir: desc
    pagination: '1'
    url_taxonomy_filters: true
---

```python
#!/usr/bin/env python
#Minecraft-Pi - Hide &amp; Seek
# Russell Barnes - 20th November 2013 for Inside Linux magazine. www.linuxinside.fr
# Adapted from Martin O'Hanlon's original code with permission. www.stuffaboutcode.com

#  Copyright 2014 Oracom.

#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.

from mcpi.minecraft import Minecraft
from mcpi import block
from mcpi.vec3 import Vec3
from time import sleep, time
import random, math

mc = Minecraft.create() # Ouvrir une connexion au jeu
playerPos = mc.player.getPos()

# fonction d'arrondi position float joueurs en position entière
def roundVec3(vec3):
    return Vec3(int(vec3.x), int(vec3.y), int(vec3.z))

# fonction de calcul rapide de distance entre points
def distanceBetweenPoints(point1, point2):
    xd = point2.x - point1.x
    yd = point2.y - point1.y
    zd = point2.z - point1.z
    return math.sqrt((xd*xd) + (yd*yd) + (zd*zd))

def random_block(): # créer un bloc dans une position aléatoire parmi les 50 blocs du joueur
    randomBlockPos = roundVec3(playerPos)
    randomBlockPos.x = random.randrange(randomBlockPos.x - 50, randomBlockPos.x + 50)
    randomBlockPos.y = random.randrange(randomBlockPos.y - 5, randomBlockPos.y + 5)
    randomBlockPos.z = random.randrange(randomBlockPos.z - 50, randomBlockPos.z + 50)
    return randomBlockPos

def main(): # la boucle principale du cache-cache
    global lastPlayerPos, playerPos
    seeking = True
    lastPlayerPos = playerPos
    #random_block()
    
    #mc.setBlock(randomBlockPos.x, randomBlockPos.y, randomBlockPos.z, DIAMOND_BLOCK)
    randomBlockPos = random_block()
    mc.setBlock(randomBlockPos, block.DIAMOND_BLOCK)
    mc.postToChat("Un diamant est caché quelque part - trouvez-le !")
    
    lastDistanceFromBlock = distanceBetweenPoints(randomBlockPos, lastPlayerPos)
    timeStarted = time()
    while seeking:
        # Prendre la position des joueurs
        playerPos = mc.player.getPos()
        # Le joueur a-t-il bougé ?
        if lastPlayerPos != playerPos:
            #print "dernièreDistanceDuBloc = " + str(lastDistanceFromBlock)
            distanceFromBlock = distanceBetweenPoints(randomBlockPos, playerPos)
            #print "distanceFromBlock = " + str(distanceFromBlock)
            if distanceFromBlock &lt; 2:
                # Trouvé !
                seeking = False
            else:
                if distanceFromBlock &lt; lastDistanceFromBlock:
                    mc.postToChat("Plus chaud de " + str(int(distanceFromBlock)) + " blocs")
                if distanceFromBlock &gt; lastDistanceFromBlock:
                    mc.postToChat("Plus froid de " + str(int(distanceFromBlock)) + " blocs")
          
            lastDistanceFromBlock = distanceFromBlock
          
        sleep(2)
        
    timeTaken = time() - timeStarted
    mc.postToChat("Bien joué - " + str(int(timeTaken)) + " secondes pour trouver le diamant")

if __name__ == "__main__":
    main()
```
