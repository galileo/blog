---
title: 'Raspberry Pi : double interface WIFI'
hide_page_title: false
show_sidebar: true
hide_git_sync_repo_link: false
content:
    items: '@self.children'
    limit: '5'
    order:
        by: date
        dir: desc
    pagination: '1'
    url_taxonomy_filters: true
---

comment exploiter un RaspberryPi disposant de plusieurs interfaces réseau

## 1 - une interface WIFI

J'ai connecté une clé usb WIFI sur le port USB du bas.  
  

  
$ lsusb  
Bus 001 Device 004: ID 2001:330d D-Link Corp.  
  

  
Le nom de l'interface créée automatiquement est wlan0  

## 2 - deux interfaces WIFI

ATTENTION : Ne branchez pas directement une clé USB WIFI sur votre raspberry
pendant son fonctionnement! Cela provoque un reboot inopiné.  
  
Je place la clé D-Link en bas et la clé NETGEAR en haut  
  
  

$ lsusb  
  
Bus 001 Device 005: ID 2001:330d D-Link Corp.  
Bus 001 Device 004: ID 0846:9043 NetGear, Inc.  

  
Le Device passe de 4 à 3 pour la clé D-LINK le nom de son interface est wlan1.  
wlan0 est attribué à la clé NetGear.  
sa consommation est d'environ au repos est d'environ 100mA soit environ
**0.5W**  

## 3 - Inversion des clés

NetGear en bas et D-Link en haut  
  

  
$ lsusb  
Bus 001 Device 005: ID 0846:9043 NetGear, Inc.  
Bus 001 Device 004: ID 2001:330d D-Link Corp.  
  

  
Les numéros de Device sont bien inversés  
L'interface wlan0 est maintenant attribuée à la clé D-Link  
La consommation mesurée lors de la commande "sudo iwlist wlan0 scan" est de
80mA soit environ **0.4W**  
