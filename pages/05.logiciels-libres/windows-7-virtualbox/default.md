---
title: 'Intégrer la licence Windows 7 OEM d’un PC sous VirtualBox'
published: true
hide_page_title: false
show_sidebar: true
hide_git_sync_repo_link: false
---

http://esver.free.fr/blog/?p=377

Posted in 2 juillet 2013

# Introduction

Le but de l’opération est de réinstaller le Windows présent à l’origine sur l’ordinateur dans une machine virtuelle sous linux et de garder l’activation de celui-ci.

Pour être activé, un Windows 7 OEM doit remplir 3 conditions :
* Avoir une table ACPI SLIC dans le BIOS,
* Avoir un certificat correspondant à cette table
* Avoir une clé correspondant au certificat et à la table.

Pour pouvoir passer cette licence sous VirtualBox il faut sauvegarder la clé et le certificat de Windows (si aucun DVD contenant Windows et ses certificats n’a été fourni avec l’ordinateur) pour les remettre dans la machine virtuelle et aussi permettre à celle-ci d’accéder à la table SLIC du BIOS. Il est ainsi possible de garder son Windows OEM dans une machine virtuelle tout en passant sa machine sous GNU/Linux.

# Sauvegarde de la clé ainsi que du certificat de Windows 7

Pour cela il suffit de lancer en administrateur l’application SLIC_ToolKit_V3.2, cliquer sur l’onglet Advanced et cliquer sur le bouton Backup pour sauvegarder la clé ainsi que le certificat.
SLIC_ToolKit_V3.2

La clé se trouve dans le fichier ProductId.txt et le certificat contient l’extension xrm-ms.


# Copie des information du BIOS dans la machine virtuelle À la main

## Copie de la table slic dans la machine virtuelle :

```sh
sudo dd if="/sys/firmware/acpi/tables/SLIC" of=Emplacement_de_la_VM
sudo chown $USER: Emplacement_de_la_VM
```

## Ajout de la table dans la VM

```sh
VBoxManage setextradata "Nom_De_La_VM" "VBoxInternal/Devices/acpi/0/Config/CustomTable" "Emplacement_de_la_VM"
```

Puis grâce à l’outil dmidecode, qui permet d’afficher certaines données du BIOS, il faut copier les valeurs Vendor et Version de BIOS Information (dmidecode -t0), Manufacturer et Product Name de System Information (dmidecode -t1) et enfin Manufacturer et Product Name de Base Board Information (dmidecode -t2) dans respectivement

```sh
VBoxInternal/Devices/pcbios/0/Config/DmiBIOSVendor
VBoxInternal/Devices/pcbios/0/Config/DmiBIOSVersion
VBoxInternal/Devices/pcbios/0/Config/DmiSystemProduct
VBoxInternal/Devices/pcbios/0/Config/DmiSystemVendor
VBoxInternal/Devices/pcbios/0/Config/DmiBoardProduct
VBoxInternal/Devices/pcbios/0/Config/DmiBoardVendor
```
En respectant la casse.

# Script Shell qui automatise tout ça sous Linux

Voici un script qui permet d’automatiser la copie du SLIC ainsi que des valeurs trouvées avec dmidecode dans la machine virtuelle :

```sh
#!/bin/sh
 
SLIC_LOCATION="/sys/firmware/acpi/tables/SLIC"
 
VM_NAME="$1"
 
if ([ -n "$VM_NAME" ]);then
  VM_IN_LIST=`VBoxManage list vms |grep "$VM_NAME"`
  if ([ -n "$VM_IN_LIST" ]);then
 
    VM_BASE_DIR=`VBoxManage showvminfo "$VM_NAME" |grep "Config file:"|sed 's/Config file://'|sed 's/^[ \t]*//'| grep -o '.*/'`
    SLIC_LOCAL_LOCATION="${VM_BASE_DIR%/}/SLIC.bin"
 
    if [ ! -e "$SLIC_LOCATION" ];then
      echo "No SLIC table found"
    elif [ -f "$SLIC_LOCATION" ];then
      echo "SLIC exist, copy to $SLIC_LOCAL_LOCATION"
      sudo dd if="$SLIC_LOCATION" of="$SLIC_LOCAL_LOCATION"
      sudo chown $USER: "$SLIC_LOCAL_LOCATION"
       
      VBoxManage setextradata "$VM_NAME" "VBoxInternal/Devices/acpi/0/Config/CustomTable"          "$SLIC_LOCAL_LOCATION"
    fi
     
    BIOS_VENDOR=`sudo dmidecode -t0| grep 'Vendor:'|sed 's/Vendor://'|sed 's/^[ \t]*//'`
    BIOS_VERSION=`sudo dmidecode -t0| grep 'Version:'|sed 's/Version://'|sed 's/^[ \t]*//'`
     
    SYSTEM_VENDOR=`sudo dmidecode -t1| grep 'Manufacturer:'|sed 's/Manufacturer://'|sed 's/^[ \t]*//'`
    SYSTEM_PRODUCT=`sudo dmidecode -t1| grep 'Product Name:'|sed 's/Product Name://'|sed 's/^[ \t]*//'`
 
    BOARD_VENDOR=`sudo dmidecode -t2| grep 'Manufacturer:'|sed 's/Manufacturer://'|sed 's/^[ \t]*//'`
    BOARD_PRODUCT=`sudo dmidecode -t2| grep 'Product Name:'|sed 's/Product Name://'|sed 's/^[ \t]*//'`
 
    VBoxManage setextradata "$VM_NAME" "VBoxInternal/Devices/pcbios/0/Config/DmiBIOSVendor"      "string:$BIOS_VENDOR"
    VBoxManage setextradata "$VM_NAME" "VBoxInternal/Devices/pcbios/0/Config/DmiBIOSVersion"     "string:$BIOS_VERSION"
    VBoxManage setextradata "$VM_NAME" "VBoxInternal/Devices/pcbios/0/Config/DmiSystemProduct"   "string:$SYSTEM_PRODUCT"
    VBoxManage setextradata "$VM_NAME" "VBoxInternal/Devices/pcbios/0/Config/DmiSystemVendor"    "string:$SYSTEM_VENDOR"
    VBoxManage setextradata "$VM_NAME" "VBoxInternal/Devices/pcbios/0/Config/DmiBoardProduct"    "string:$BOARD_PRODUCT"
    VBoxManage setextradata "$VM_NAME" "VBoxInternal/Devices/pcbios/0/Config/DmiBoardVendor"     "string:$BOARD_VENDOR"
  else
    echo "VM not found"
  fi
else
  echo "Use VM_ACPI.sh \"VM to modify\""
  VBoxManage list vms
fi
```
# Script BATCH qui automatise tout ça sous Windows (ou presque)

Voici un script qui permet d’automatiser la copie du SLIC ainsi que des valeurs trouvées avec dmidecode dans la machine virtuelle, il faut préalablement avoir récupéré la table SLIC dans un fichier avec l’outil SLIC_ToolKit_V3.2 (fichier renommé en SLIC.BIN dans l’exemple ci dessous) et avoir récupérés les outils grep, sed et DmiDecode ainsi que leurs dépendances sur GnuWin32.
Il faut aussi rajouter le PATH vers VBoxManage (souvent C:\Program Files\Oracle\VirtualBox).

```sh
@echo off
rem Add to path : C:\Program Files\Oracle\VirtualBox
rem setlocal for local variable
rem enabledelayedexpansion remplace % with ! in if/else
setlocal enabledelayedexpansion
set SLIC_NAME=SLIC.BIN
 
set VM_NAME=%1
 
if not [%VM_NAME%] == [] (
  for /F "delims=" %%V in ('VBoxManage list vms ^| grep %VM_NAME%') do set VM_IN_LIST=%%V
  if not [!VM_IN_LIST!] == [] (
 
    rem ~pdV : p pour lettre de lecteur et d pour repertoires cf http://www.computerhope.com/FORhlp.htm
    for /F "delims=" %%V in ('VBoxManage showvminfo !VM_NAME! ^|grep "Config file:"^|sed "s/Config file://"^|sed "s/^[ \t]*//"') do set VM_BASE_DIR=%%~pdV
    set SLIC_LOCAL_LOCATION=!VM_BASE_DIR!!SLIC_NAME!
 
    if exist !SLIC_NAME! (
      echo SLIC exist, copy to $SLIC_LOCAL_LOCATION
      copy !SLIC_NAME! "!SLIC_LOCAL_LOCATION!"
      VBoxManage setextradata !VM_NAME! "VBoxInternal/Devices/acpi/0/Config/CustomTable"          "!VM_BASE_DIR!!SLIC_NAME!"
    ) else (
      echo No SLIC table found
    )
 
    for /F "delims=" %%V in ('dmidecode -t0^| grep "Vendor:"^|sed "s/Vendor://"^|sed "s/^[ \t]*//"') do set BIOS_VENDOR=%%V
    for /F "delims=" %%V in ('dmidecode -t0^| grep "Version:"^|sed "s/Version://"^|sed "s/^[ \t]*//"') do set BIOS_VERSION=%%V
 
    for /F "delims=" %%V in ('dmidecode -t1^| grep "Manufacturer:"^|sed "s/Manufacturer://"^|sed "s/^[ \t]*//"') do set SYSTEM_VENDOR=%%V
    for /F "delims=" %%V in ('dmidecode -t1^| grep "Product Name:"^|sed "s/Product Name://"^|sed "s/^[ \t]*//"') do set SYSTEM_PRODUCT=%%V
 
    for /F "delims=" %%V in ('dmidecode -t2^| grep "Manufacturer:"^|sed "s/Manufacturer://"^|sed "s/^[ \t]*//"') do set BOARD_VENDOR=%%V
    for /F "delims=" %%V in ('dmidecode -t2^| grep "Product Name:"^|sed "s/Product Name://"^|sed "s/^[ \t]*//"') do set BOARD_PRODUCT=%%V
 
    echo Set BOARD_PRODUCT to "!BOARD_PRODUCT!"
    VBoxManage setextradata !VM_NAME! "VBoxInternal/Devices/pcbios/0/Config/DmiBoardProduct"    "string:!BOARD_PRODUCT!"
    echo Set BOARD_VENDOR to "!BOARD_VENDOR!"
    VBoxManage setextradata !VM_NAME! "VBoxInternal/Devices/pcbios/0/Config/DmiBoardVendor"     "string:!BOARD_VENDOR!"
 
    echo Set SYSTEM_PRODUCT to "!SYSTEM_PRODUCT!"
    VBoxManage setextradata !VM_NAME! "VBoxInternal/Devices/pcbios/0/Config/DmiSystemProduct"   "string:!SYSTEM_PRODUCT!"
    echo Set SYSTEM_VENDOR to "!SYSTEM_VENDOR!"
    VBoxManage setextradata !VM_NAME! "VBoxInternal/Devices/pcbios/0/Config/DmiSystemVendor"    "string:!SYSTEM_VENDOR!"
   
    echo Set BIOS_VENDOR to "!BIOS_VENDOR!"
    VBoxManage setextradata !VM_NAME! "VBoxInternal/Devices/pcbios/0/Config/DmiBIOSVendor"      "string:!BIOS_VENDOR!"
    echo Set BIOS_VERSION to "!BIOS_VERSION!"
    VBoxManage setextradata !VM_NAME! "VBoxInternal/Devices/pcbios/0/Config/DmiBIOSVersion"     "string:!BIOS_VERSION!"
 
  ) else (
  echo VM not found
  )
) else (
  echo Use slicadd.cmd "VM to modify"
  VBoxManage list vms 
)
```

# Activer Windows en lui mettant la clé et le certificat À la main

Ajout du certificat

```sh
slmgr -ilc Chemin\vers\le\certificat.xrm-ms
```

Ajout de la clé

```sh
slmgr -ipk XXXXX-XXXXX-XXXXX-XXXXX-XXXXX
```

# Script BATCH qui automatise tout ça

Voici un script windows, à modifier en fonction de la clé et du certificat sauvegardé précédemment, qui permet de copier le certificat ainsi que la clé (les chemins ne doivent pas contenir d’espaces) à lancer en administrateur :

```sh
@echo off
setlocal enableextensions
echo "Installation du certificat"
slmgr -ilc "%~dp0\DELL_V2.1_Cert.xrm-ms"
echo "Installation de la cle"
slmgr -ipk XXXXX-XXXXX-XXXXX-XXXXX-XXXXX
```
