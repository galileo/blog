---
title: 'RASPBIAN su DD'
hide_page_title: false
show_sidebar: true
hide_git_sync_repo_link: false
---

## heading<a name="headin"></a>


# cmdline.txt
dwc_otg.lpm_enable=0 console=tty1 root=PARTUUID=19def283-8b7d-4bbb-824f-a62cb7291a6a rootfstype=ext4 elevator=deadline fsck.repair=yes rootwait rootdelay=10

# /etc/fstab 
proc            /proc           proc    defaults          0       0
/dev/mmcblk0p1  /boot           vfat    defaults          0       2
'#/dev/mmcblk0p2  /               ext4    defaults,noatime  0       1

UUID=bf50a91f-28fe-4a6e-aede-e1dd6a1ffbc1	/               ext4    defaults,noatime  0       1 #voir 
UUID=c82aa144-6d22-440e-aa53-52b97b97b84b	none            swap    sw              0       0
UUID=30911d88-165f-4d01-97ad-91bdc6236312	/home		ext4    defaults        0       2

'# a swapfile is not a swap partition, no line here
'#   use  dphys-swapfile swap[on|off]  for that


# ll /dev/disk/by-partuuid/
total 0
drwxr-xr-x 2 root root 140 févr. 11 11:38 .
drwxr-xr-x 7 root root 140 févr. 11 11:38 ..
lrwxrwxrwx 1 root root  10 févr. 14 05:50 19def283-8b7d-4bbb-824f-a62cb7291a6a -> ../../sda1
lrwxrwxrwx 1 root root  15 févr. 11 11:38 3e788dad-01 -> ../../mmcblk0p1
lrwxrwxrwx 1 root root  15 févr. 11 11:38 3e788dad-02 -> ../../mmcblk0p2
lrwxrwxrwx 1 root root  10 févr. 14 05:50 67b928f4-38d1-44d4-8fe4-4e9fa639926f -> ../../sda2
lrwxrwxrwx 1 root root  10 févr. 14 05:50 d509e2e9-14c9-4017-bc62-4d28f6abf3b8 -> ../../sda3


# ll /dev/disk/by-uuid/
total 0
drwxr-xr-x 2 root root 140 févr. 11 11:38 .
drwxr-xr-x 7 root root 140 févr. 11 11:38 ..
lrwxrwxrwx 1 root root  15 févr. 11 11:38 0aed834e-8c8f-412d-a276-a265dc676112 -> ../../mmcblk0p2
lrwxrwxrwx 1 root root  15 févr. 11 11:38 0F5F-3CD8 -> ../../mmcblk0p1
lrwxrwxrwx 1 root root  10 févr. 14 05:50 30911d88-165f-4d01-97ad-91bdc6236312 -> ../../sda3
lrwxrwxrwx 1 root root  10 févr. 14 05:50 bf50a91f-28fe-4a6e-aede-e1dd6a1ffbc1 -> ../../sda1
lrwxrwxrwx 1 root root  10 févr. 14 05:50 c82aa144-6d22-440e-aa53-52b97b97b84b -> ../../sda2



# gdisk

Partition number (1-3): 1
Partition GUID code: 0FC63DAF-8483-4772-8E79-3D69D8477DE4 (Linux filesystem)
Partition unique GUID: 19DEF283-8B7D-4BBB-824F-A62CB7291A6A


Partition number (1-3): 2
Partition GUID code: 0657FD6D-A4AB-43C4-84E5-0933C84B4F4F (Linux swap)
Partition unique GUID: 67B928F4-38D1-44D4-8FE4-4E9FA639926F

Partition number (1-3): 3
Partition GUID code: 0FC63DAF-8483-4772-8E79-3D69D8477DE4 (Linux filesystem)
Partition unique GUID: D509E2E9-14C9-4017-BC62-4D28F6ABF3B8


[This is the link text](#headin)

