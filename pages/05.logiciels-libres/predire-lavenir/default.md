---
title: 'Prédire l''avenir'
hide_page_title: false
show_sidebar: true
hide_git_sync_repo_link: false
content:
    items: '@self.children'
    limit: '5'
    order:
        by: date
        dir: desc
    pagination: '1'
    url_taxonomy_filters: true
---

Il est très difficile de prédire l'avenir ceux qui ont essayé et qui se sont trompés peuvent essayer de se cacher mais malheureusement internet a de la mémoire

En 2010 le petit P4nd4 écrivait au sujet des clusters
http://p4nd1-p4nd4.over-blog.com/pages/Super-ordinateurs-linux-vs-windows---get-the-facts-3175238.html

> Si l’on tient compte que la nouvelle version de 2008 HPC R2 est plus puissante, et surtout infiniment plus simple à mettre en œuvre, le long terme devrait marquer une croissance extrêmement spectaculaire pour les systèmes d’exploitation de Microsoft


En 2017, qu'en est-il de Windows sur les clusters
https://www.top500.org/statistics/sublist/

Vous aurez beau cherché, windows a définitivement disparue du TOP500

get the facts!
