---
title: 'Comparer ce qui est comparable'
hide_page_title: false
show_sidebar: true
hide_git_sync_repo_link: false
content:
    items: '@self.children'
    limit: '5'
    order:
        by: date
        dir: desc
    pagination: '1'
    url_taxonomy_filters: true
---

Je suis tombé par hasard sur cet article :

http://p4nd1-p4nd4.over-blog.com/pages/Windows_Vs_Linux_Vision_dun_utilisateur_sur_un_NetBook-2582762.html

Il semble que ce test date de 2009.
On peut y relever quelques biais :

# Pas de son :
faut il incriminer les développeurs ou l'utilisateur?

# OS :
je pense KUBUNTU est plus gourmand en ressources que XUBUNTU

# Open Office :
"(tous les binaires multilingues ne sont pas présents sur le CD de Kubuntu…);"
Je ne vois pas ceque cela vient faire dans un test de performance
Les binaires multilingues sont ils disponible pour la suite bureautique windows (on me souffle à l'oreille que non)

# Taille disque : 
quelle est la taille occupée sur le disque dur par windows et par linux?

# Flash player:
Pourquoi tester une machine avec un logiciel qui n'est pas réputé pour être optimisé sur linux !

# Temps de démarrage / mémoire consommée :
Il faudrait peut être préciser les services actifs au démarrage

# IE / firefox :
il aurait été plus judicieux de tester le même logiciel sur les deux plateformes 

# Lecture de fichiers vidéo :
Les options d'optimisation processeur et gestion de l'énergie ont-elles été configurées sur Linux

# Bilan :
On voit nettement le positionnement d'un utilisateur de Windows qui ne maitrise pas Linux et qui rejette sur l'OS son incompétence.

Signalons que la configuration Windows ne possède pas d'anti virus. il serait intéressant de refaire les tests, après d'une utilisation classique des 2 OS (surf, bureautique, ...) après 2 ou 3 mois. Windows démarrera-t-il toujours en 40 secondes ? IE pourra-t-il ouvrir rapidement 10 onglets? J'en doute fortement!

Cette page m'a donné envie de faire un test comparatif. Je vais ressortir une veille machine et lancer une batterie de test en étant le plus imparciale possible.
La suite dans une prochaine page.
