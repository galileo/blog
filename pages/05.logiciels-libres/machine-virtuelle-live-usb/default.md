---
title: 'Machine Virtuelle LIVE-USB'
hide_page_title: false
show_sidebar: true
hide_git_sync_repo_link: false
content:
    items: '@self.children'
    limit: '5'
    order:
        by: date
        dir: desc
    pagination: '1'
    url_taxonomy_filters: true
---

comment tester une clé live usb avec KVM?  
  

## Identification  

La première étape consiste à identifier la clé USB à tester  

$ lsusb

Bus 003 Device 010: ID 090c:1000 Silicon Motion, Inc. - Taiwan Flash Drive

  
id vendor = 090c  
id model = 1000  

## VM

Pour lancer la machine virtuelle, il suffit de reprendre ces informations  
  

$ kvm -m 512 -smp 1 -usb -usbdevice host:090c:1000 -boot menu=on -vga cirrus
