---
title: 'Pourquoi GNU/Linux'
hide_page_title: false
show_sidebar: true
hide_git_sync_repo_link: false
content:
    items: '@self.children'
    limit: '5'
    order:
        by: date
        dir: desc
    pagination: '1'
    url_taxonomy_filters: true
---

Je fais la promotion de GNU/LINUX.
On me pose souvent les mêmes questions au sujet de LINUX.

Ce site répondra rapidement aux questions que vous vous posez.

[http://getgnulinux.org/](http://getgnulinux.org/)
