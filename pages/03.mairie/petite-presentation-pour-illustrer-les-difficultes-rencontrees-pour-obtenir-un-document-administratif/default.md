---
title: 'Petite présentation pour illustrer les difficultés rencontrées pour obtenir un document administratif (BIS)'
published: true
date: '06-10-2017 00:00'
hide_page_title: false
show_sidebar: true
hide_git_sync_repo_link: false
content:
    items: '@self.children'
    limit: '5'
    order:
        by: date
        dir: desc
    pagination: '1'
    url_taxonomy_filters: true
---

* 7 mars 2014 courriel à mairie avec copie conseillers<br /><br />Auriez vous la possibilité de fournir le PV, non signé, au format bureautique standard (ODT ou DOCX).<br />En effet, cela facilite le classement, l'indexation et la recherche dans un archivage informatisé.<br />Je vous en remercie par avance.<br />M. VALLAS<br /><br />* 7 mars 2014 réponse à moi<br />J’ai le regret de vous informer que votre demande est illégale et que je ne peux donc la satisfaire.<br />En effet, cela comporterait le risque que ce document soit utilisé et/ou modifié.<br /><br />* 7 mars 2014 courriel à mairie seule<br />Lorsque vous déclarez une demande illégale veuillez s'il vous plaît citer le texte de loi qui y fait référence afin de valider vos propos et d'améliorer ma culture.<br />Je peux techniquement numériser ce document pour en extraire le texte mais ce processus est relativement long alors que vous le possédez déjà au format numérique, de plus il serait source d'erreurs.<br />Merci de bien vouloir réétudier ma demande car, si elle est légale, vous n'avez aucune raison de vous y opposer.
Cordialement
M. VALLAS<br /><br />
"Toute personne physique ou morale a le droit de demander communication des procès-verbaux du conseil municipal, des budgets et des comptes de la commune et des arrêtés municipaux.
http://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006070633&amp;idArticle=LEGIARTI000006389886&amp;dateTexte=&amp;categorieLien=cid<br /><br />Loi n° 78-753 du 17 juillet 1978
Chapitre Ier : De la liberté d'accès aux documents administratifs.
" L'accès aux documents administratifs s'exerce, au choix du demandeur et dans la limite des possibilités techniques de l'administration :...
...Par courrier électronique et sans frais lorsque le document est disponible sous forme électronique."

http://www.legifrance.gouv.fr/affichTexte.do;jsessionid=7448B69CC317B62EF1E3F728739CF629.tpdjo01v_3&amp;dateTexte=?cidTexte=JORFTEXT000000339241&amp;categorieLien=cid<br /><br /><br />