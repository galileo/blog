---
title: datalove
published: true
date: '07-10-2017 00:00'
hide_page_title: false
show_sidebar: true
hide_git_sync_repo_link: false
content:
    items: '@self.children'
    limit: '5'
    order:
        by: date
        dir: desc
    pagination: '1'
    url_taxonomy_filters: true
---

Ma démarche s'inscrit dans les principes du <a href="http://datalove.fr/" title="DATALOVE">DATALOVE</a> et le mouvement d'ouverture des données publiques.

<br /><span class="datalove"></span><ul><li>Les données sont essentielles</li><li>Les données doivent circuler</li><li>Les données doivent être utilisées</li><li>Les données ne sont ni bonnes, ni mauvaises</li><li>Il n'y a pas de données illégales</li><li>Les données sont libres</li><li>Les données ne peuvent être possédées</li><li>Aucun homme, machine ou système ne doit interrompre le flux de données</li><li>Verrouiller les données est un crime contre la datanité</li></ul><span class="datalove"></span><span class="datalove"></span><br />