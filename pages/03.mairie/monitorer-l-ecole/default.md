---
title: 'monitorer l''école'
published: true
date: '09-02-2017 00:00'
hide_page_title: false
show_sidebar: true
hide_git_sync_repo_link: false
content:
    items: '@self.children'
    limit: '5'
    order:
        by: date
        dir: desc
    pagination: '1'
    url_taxonomy_filters: true
---

Comment rendre une école élémentaire-primaire plus économe en énergie?

# Le contexte
La municipalité constate que la facture énergétique des bâtiment est très importante.
# Eléments de réflexion
Pour mener à bien un projet de réduction de la dépense énergétique, bous pouvons nous appuyer sur plusieurs éléments
1. Métrologie
* La température des pièces
* Température extérieure
* Prévision de température de la nuit/lendemain
* détection de la luminosité (éclairages oubliés)
* variations de température dans la journée
* ouvertures des portes extérieures intérieures
* le relevé téléinformation EDF
* relevé de consommation avec des pinces ampèremétriques

Un dépot [GITHUB](https://github.com/nvallas/econom) pour travailler en groupe
