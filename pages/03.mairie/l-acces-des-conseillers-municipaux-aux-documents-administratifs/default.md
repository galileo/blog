---
title: 'L''accès des conseillers municipaux aux documents administratifs'
published: true
date: '17-10-2017 00:00'
hide_page_title: false
show_sidebar: true
hide_git_sync_repo_link: false
content:
    items: '@self.children'
    limit: '5'
    order:
        by: date
        dir: desc
    pagination: '1'
    url_taxonomy_filters: true
---

**Quelles sont les modalités d'accès aux document communaux pour les citoyens et spécifiquement pour les conseillers municipaux.**
    
http://www.maires-isere.fr/Dossiers_juridiques/Dossiers%20conseils%20aux%20%C3%A9lusGestion%20locale%20-%20Communication%20des%20documents%20administratifs%20communaux.htm

    Le Conseil d'État avait confirmé que la liste 	limitative de l'article L.121-19 du code des communes remplacé par l'article L.2121-26 du code général des collectivités territoriales n'était pas opposable aux membres d'un conseil municipal, non pourvus de délégation du maire puisque ces derniers ont "un droit à être informés de tout ce qui touche les affaires de la commune et ce, par l'intermédiaire du Maire" (CE, Ass, 9 novembre 1973, commune de Point-à-Pitre).
	
    En fait, les conseillers municipaux disposent d'un droit spécifique à la communication des documents communaux par le maire. L'article L.2121-13 du code général des collectivités territoriales dispose en effet que "tout membre du conseil municipal a le droit, dans le cadre de sa fonction, d'être informé des affaires de la commune qui font l'objet d'une délibération".
    
	La loi n'imposant pas au maire de fournir à chacun des conseillers municipaux la copie intégrale des dossiers examinés en séance, la facturation de copie demandée par les conseillers n'apparaît pas illégale. 
	Le maire ne peut, en tout état de cause, refuser de communiquer ces documents avant la réunion du conseil aux conseillers qui souhaitent les consulter, un refus de sa part pouvant, selon le Conseil d'État (CE, 29 juin 1990, commune de Guitrancourt) entacher d'illégalité la délibération prise sur l'affaire en cause.
	
    NOTA : Selon l’article 25 de l’ordonnance du 6 
	juin 2005, toute décision de refus d’accès aux documents administratifs est 
	notifiée au demandeur sous la forme d’une décision écrite motivée comportant 
	l’indication des voies et délais de recours.</font> </p><br />
    