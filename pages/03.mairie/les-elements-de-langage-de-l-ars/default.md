---
title: 'Les éléments de langage de l''ARS'
published: true
hide_page_title: false
show_sidebar: true
hide_git_sync_repo_link: false
content:
    items: '@self.children'
    limit: '5'
    order:
        by: date
        dir: desc
    pagination: '1'
    url_taxonomy_filters: true
---

**De :** [ARS-DT72-SSPE@ars.sante.fr](mailto:ARS-DT72-SSPE@ars.sante.fr) [[mailto:ARS-DT72-SSPE@ars.sante.fr](mailto:ARS-DT72-SSPE@ars.sante.fr)]   
**Envoyé :** mercredi 1 février 2017 16:10  
**À :** [siaep.brettelespins@wanadoo.fr](mailto:siaep.brettelespins@wanadoo.fr); 

**Objet :** Communication ARS - Qualité eau potable

 _A l’attention des personnes responsables de la production et de la
distribution d’eau potable et des exploitants :_

Suite à la publication de la carte UFC Que Choisir  et aux articles parus dans
la presse concernant la qualité d’eau distribuée sur les communes du
département de la Sarthe, l’Agence Régionale de Santé vous invite à
communiquer et diffuser  largement auprès de vos abonnés, les différents
**éléments de langage** concernant le bilan de la qualité de l’eau 2015 avec
également l’évolution des pesticides sur la période 2015-2016.



 Vous trouverez sur notre site internet ARS des Pays de la Loire cette
information en cliquant directement sur le lien suivant :



[https://www.pays-de-la-loire.ars.sante.fr/eau-potable-bilan-2015-99-des-
prelevements-respectent-les-exigences-de-qualite](https://www.pays-de-la-
loire.ars.sante.fr/eau-potable-bilan-2015-99-des-prelevements-respectent-les-
exigences-de-qualite)



 Cordialement,

  

Sécurité sanitaire des personnes et de l’environnement**  
**Délégation territoriale de la Sarthe

_________________________________________________ **  
**l **Agence régionale de santé Pays-de-la-Loire****  
**19, Boulevard Paixhans - CS 71914 - 72019 LE MANS cedex 9

Secrétariat : [02 44 81 30 32](tel:02%2044%2081%2030%2032)

_www.ars.paysdelaloire.sante.fr_ \- _ars-dt72-sspe@ars.sante.fr_

