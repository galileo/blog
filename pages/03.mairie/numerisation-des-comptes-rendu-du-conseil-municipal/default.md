---
title: 'Numérisation des comptes rendu du conseil municipal'
published: true
hide_page_title: false
show_sidebar: true
hide_git_sync_repo_link: false
visible: true
content:
    items: '@self.children'
    limit: '5'
    order:
        by: date
        dir: desc
    pagination: '1'
    url_taxonomy_filters: true
---

Laurent TAUPIN, le maire de ma commune ayant refusé de me transmettre les procès verbaux du conseil municipal au format PDF texte, j'ai décidé de me lancer dans la numérisation de tous les documents en ma possession en commençant par les compte-rendu.

La procédure est assez simple en ligne de commande, elle ne nécessite que quelques logiciels.

```sh
$ sudo apt-get install evince
$ sudo apt-get install poppler-utils
$ sudo apt-get install imagemagick
$ sudo apt-get install tesseract-ocr tesseract-ocr-fra
$ sudo apt-get eog
```

evince permet de visualier des fichiers PDF
poppler-utils contient l'outil (pdfimages) de transformation des pdf en images
imagemagick contient "convert" l'outil qui nous servira à tourner des pages mal orientées
l'outil tesseract converti une image en texte
eog (Eye of GNOME) est l'outil de visualisation des images

# 1 - Visualisation du document
La première étape consiste a déterminer si le contenu du document peut être récupéré par un simple copier/coller ou si il s'agit d'un document scanné.

```sh
evince 12\ -\ decembre\ 2013.pdf
```

# 2 - Extraction des images
La commande pdfimages permet d'extraire une à une les pages du document au format image (ppm ou pbm)

```sh
$ pdfimages 12\ -\ decembre\ 2013.pdf 2013-12-01
```

pdfimages a produit un ensemble de fichier ppm et pbm

```sh
$ ls
2013-12-01-000.ppm 2013-12-01-000.pbm 2013-12-01-000.pbm 2013-12-01-000.pbm 2013-12-01-000.ppm
```

# 3 - Vérification de l'orientation
Suivant la méthode utilisé pour le scan, il peut être nécessaire d'appliquer une rotation aux images extraites.

```sh
$ eog 2013-12-01-004.pbm
```

Dans notre cas, une rotation à 270° est nécessaire

# 4 - rotation

La rotation s'effectue grâce à la commande "convert"

```sh
$ for i in *.pbm ; do convert $i -rotate 270 $i.tif;done
$ for i in *.ppm ; do convert $i -rotate 270 $i.tif;done
```

# 5 - OCR

La reconnaissance optique de caractères (ROC), en anglais "optical character recognition" (OCR), permet de convertir des image en document texte
```sh
$ for i in *.tif ; do tesseract $i $i -l fra;done
```

A la fin du processus nous obtenons un fichier texte par page

```sh
$ ls *.txt
2013-12-01-000.ppm.tif.txt 2013-12-01-003.pbm.tif.txt 2013-12-01-006.pbm.tif.txt 2013-12-01-009.pbm.tif.txt 2013-12-01-012.pbm.tif.txt
2013-12-01-001.ppm.tif.txt 2013-12-01-004.pbm.tif.txt 2013-12-01-007.pbm.tif.txt 2013-12-01-010.pbm.tif.txt 2013-12-01-013.ppm.tif.txt
2013-12-01-002.pbm.tif.txt 2013-12-01-005.pbm.tif.txt 2013-12-01-008.pbm.tif.txt 2013-12-01-011.ppm.tif.txt
```

il nous reste à créer un fichier texte rassemblant tous ces élément

```sh
$ cat *.txt > total.txt
```

Le fichier obtenu est utilisable pour intégrer un site web ou une base de donnée.

Un exemple peut être consulté à cette adresse :
[https://saintmarsdoutille.info/category/municipalite/compte-rendu-des-conseils-municipaux/](Exemple)