---
title: 'Demandes abusives'
published: true
date: '06-10-2017 00:00'
hide_page_title: false
show_sidebar: true
hide_git_sync_repo_link: false
content:
    items: '@self.children'
    limit: '5'
    order:
        by: date
        dir: desc
    pagination: '1'
    url_taxonomy_filters: true
---

<a href="http://www.cada.fr/les-demandes-abusives,6147.html" title="http://www.cada.fr/les-demandes-abusives,6147.html">http://www.cada.fr/les-demandes-abusives,6147.html</a><h1>Les demandes abusives<br /></h1><p>L’article 2 de la loi du 17 juillet 1978 prévoit 
expressément que «&nbsp;l’administration n’est pas tenue de donner suite aux 
demandes abusives, en particulier par leur nombre, leur caractère 
répétitif ou systématique&nbsp;». La CADA considère que cette règle de bonne 
administration s’applique à tous les régimes de communication, même 
lorsque le législateur ne l’a pas expressément prévu ou qu’il a 
limitativement énuméré les motifs légaux de refus. Il en va ainsi en 
matière d’informations relatives à l’environnement, ou encore dans le 
cadre des dispositions du code général des collectivités territoriales.</p>

<p><strong>Une demande est abusive lorsqu’elle a manifestement pour objet de perturber le fonctionnement du service public.</strong>
 Pour qu’elle soit déclarée comme telle par la CADA, il faut, tout 
d’abord, que l’administration s’en prévale, de préférence par écrit. Si 
cette condition est remplie, la CADA mobilise le faisceau d’indices 
suivant&nbsp;:</p>
<ul class="spip"><li> le nombre de demandes et le volume de documents demandé&nbsp;;</li><li>
 le caractère répétitif et systématique des demandes, notamment sur un 
même sujet (TA Melun, 29 mars 2000, Syndicat FO des communaux de Thiers 
pour la demande de communication faite par un syndicat de tous les 
arrêtés d’une même année relatifs au personnel communal)&nbsp;;</li><li> la volonté de nuire à l’administration (<a href="http://www.cada.fr/avis-20084654,20084654.html" class="spip_in">avis n°&nbsp;20084654</a> du 23 décembre 2008) ou de la mettre, eu égard à son importance, dans l’impossibilité matérielle de traiter les demandes&nbsp;;</li><li>
 la possibilité qu’a ou qu’a eu le demandeur d’accéder au document dans 
un passé proche&nbsp;: cas où un demandeur a déjà pris connaissance, quelques
 mois auparavant, du dossier auquel il demande accès&nbsp;; cas où le 
demandeur produit, à l’appui de sa requête devant le Conseil d’État, la 
copie des documents dont il demandait communication (CE, 8 janvier 1988,
 Van Overbeck)&nbsp;; demandes d’élus locaux qui disposent d’un droit d’accès
 privilégié en vertu des articles L. 2121-13 et 2121-13-1 du code 
général des collectivités territoriales&nbsp;;</li><li> l’existence d’un contexte tendu voire de contentieux multiples entre le demandeur et l’administration saisie&nbsp;;</li><li>
 le refus de l’intéressé de payer les frais qui lui ont été demandés à 
l’occasion de précédentes communications.
Les demandes émanant d’associations ou de syndicats font 
traditionnellement l’objet d’un examen plus souple, eu égard à la nature
 de ces organismes et à leur objet (<a href="http://www.cada.fr/conseil-20072650,20072650.html" class="spip_in">conseil n°&nbsp;20072650</a> du 5 juillet 2007).</li></ul>

<p><strong>Le caractère abusif s’apprécie demande par demande, et non en considération du demandeur lui-même</strong>.
 Ce dernier ne peut se voir priver, de manière générale, de son droit 
d’accès. Il lui appartient en revanche d’exercer avec discernement ce 
droit, ce que la Commission rappelle lorsqu’elle estime que les 
sujétions que le demandeur fait peser sur l’administration pourraient, 
si elles se poursuivaient, excéder celles que le législateur a entendu 
mettre à leur charge.</p>

<p><strong>De manière générale, la Commission recommande de privilégier 
un aménagement des modalités de communication compatibles avec le bon 
fonctionnement de l’administration</strong>, notamment par un 
échelonnement dans le temps et la recherche d’une solution négociée 
entre le demandeur et l’administration sollicitée, plutôt que d’opposer 
le caractère abusif de la demande. En aucun cas l’administration ne peut
 recourir à la notion de demande abusive pour «&nbsp;contingenter&nbsp;» a priori 
le nombre annuel de demandes de communication émanant d’une même 
personne (<a href="http://www.cada.fr/avis-20090004,20090004.html" class="spip_in">avis n°&nbsp;20090004</a> du 15 janvier 2009).</p>